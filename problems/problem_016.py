# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

from operator import truediv
from re import X


def is_inside_bounds(x, y):
    if x <=10 and x >=0 and y <=10 and y >=0:
        return True
    else:
        return False

# pass
