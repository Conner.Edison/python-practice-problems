# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.
# There is pseudocode to guide you.

def has_quorum(attendees_list, members_list):
    num_attendees = len(attendees_list)
    num_members = len(members_list)
    in_attendance = num_attendees / num_members

    if in_attendance >= 0.5:
        return True

    else:
        return False
