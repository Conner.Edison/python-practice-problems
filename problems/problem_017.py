def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    # If x is greater than or equal to rect_x
    # and y is greater than or equal to rect_y
    # and x is less than or equal to rect_x + rect_width
    # and y is less than or equal to rect_y + rect_height
    if (                                                # solution
        x >= rect_x                                     # solution
        and y >= rect_y                                 # solution
        and x <= rect_x + rect_width                    # solution
        and y <= rect_y + rect_height                   # solution
    ):                                                  # solution
        # return True
        return True                                     # solution
    # Otherwise
    else:                                               # solution
        # return False
        return False 